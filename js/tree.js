
// =================================================
// Tree
// -------------------------------------------------

/**
 *   as time passes, grow existing geometry from min to max size, and add more
 *   geometry (s.a. branches)
 *
 *   trunk:
 *      age: 0+ (days)
 *      maturityAge: 10*365 (days) (stops growing past this age)
 *
 *      maxRadius: 3 (scaled by min(1, age/maturityAge) )
 *      maxHeight: 20 (scaled by min(1, age/maturityAge) )
 *
 *      time-based events:
 *          events: [
 *              { age: [min,max], chance: 1.0, action: function (branch) { ... } }
 *          ]
 */

var Tree = Tree || function (settings, position, rotation) {
    this.settings = settings;

    this.position = position
        ? position
        : new THREE.Vector3(0, 0, 0);

    this.rotation = rotation
        ? rotation
        : new THREE.Matrix4();

    this.parts = [];

    this.containers = {};

    // add initial trunk with leaves to the tree
    var newTrunk = this.settings.factories.trunk.create(this, {
        rotation: this.rotation
    });
    newTrunk.parts.push(this.settings.factories.leaf.create(this, {
        rotation: this.rotation
    }));
    this.parts.push(newTrunk);
};

Tree.prototype = {
    growOlder: function () {
        var totalParts = 0;

        this.parts.forEach(function (part) {
            totalParts += part.growOlder(0);
        });

        return totalParts;
    },

    container: function (type) {
        if (!this.containers[type]) {
            this.containers[type] = new THREE.Geometry();
        }
        return this.containers[type];
    },

    clearContainers: function () {
        this.containers = {};
    },

    generate: function (container) {
        var totalMeshes = 0,
            that = this;

        this.clearContainers();

        this.parts.forEach(function (part, index) {
            totalMeshes += part.generate(that.position);
            container.add(part.generateMesh());
        });

        var tempLeaf = this.settings.factories.leaf.create(this);
        container.add(tempLeaf.generateMesh());

        return totalMeshes;
    }
};

Tree.Factory = function (settings) {
    this.treeId = 1000;

    this.settings = {
        branchRadians: Math.PI,

        levels: 4,

        factories: {
            trunk: null,
            leaf: null
        }
    };

    THREE.extend(this.settings, settings || {});

    // use default factories if none given
    if (!this.settings.factories.trunk) {
        this.settings.factories.trunk = new Tree.Trunk.Factory();
    }

    if (!this.settings.factories.leaf) {
        this.settings.factories.leaf = new Tree.Leaf.Factory();
    }
};

Tree.Factory.prototype = {
    create: function (position, rotation) {
        var tree = new Tree(this.settings, position, rotation);
//console.log('creating tree ', this.treeId++, tree);
        return tree;
    }
};


// =================================================
// Tree.Transforms
// -------------------------------------------------

Tree.Transforms = {
    linear: function (age, maxAge, min, max) {
        age = Math.min(age, maxAge);
        return age/maxAge * (max - min) + min;
    },

    expDecay: function (age, maxAge, min, max) {
        var cappedAge = Math.min(age, maxAge),
            factor = (1.0 - Math.exp(-cappedAge/(maxAge/4.0)));
        return factor * (max - min) + min;
    },

    expDecayFast: function (age, maxAge, min, max) {
        var cappedAge = Math.min(age, maxAge),
            factor = (1.0 - Math.exp(-cappedAge/(maxAge/2.0)));
        return factor * (max - min) + min;
    }
};


// =================================================
// Tree.Trunk
// -------------------------------------------------

Tree.Trunk = function (settings, tree, config) {
    this.settings = settings;
    this.tree = tree;

    var height = (typeof this.settings.height === 'function')
        ? this.settings.height()
        : this.settings.height,
        radius = (typeof this.settings.radius === 'function')
            ? this.settings.radius()
            : this.settings.radius;

    this.config = {
        height: height,
        radius: radius,
        parentHeightScale: 1.0, // 1.0 = appear at top end of parent trunk
        rotation: null
    };

    THREE.extend(this.config, config || {});

    if (!this.config.rotation) {
        this.config.rotation = new THREE.Matrix4();
    }

    this.age = 0;
    this.hasTrunks = false;

    this.parts = [];
};

Tree.Trunk.defaultSettings = {
    growth: {
        height: Tree.Transforms.expDecay,
        radius: Tree.Transforms.expDecay,
        untilAge: 365
    },

    newGrowths: [
        { age: 30, chance: 1.0, heightMin: 1.0, heightMax: 1.0 },
        { age: 50, chance: 0.75, heightMin: 0.5, heightMax: 1.0 },
        { age: 80, chance: 1.0, heightMin: 0.5, heightMax: 1.0 }
    ],

    scale: {
        height: 0.6,
        radius: 0.75
    },

    generateOptions: {
        material: new THREE.MeshPhongMaterial({color: 0x996600}),
        radiusSegments: 8,
        heightSegments: 1
    }
};

Tree.Trunk.prototype = {
    growOlder: function (level) {
        var treeSettings = this.tree.settings,
            branchRadians = treeSettings.branchRadians,
            trunkSettings = this.settings,
            config = this.config,
            that = this,
            newHeight,
            newRadius,
            rotation,
            totalParts = 1;
//console.log('growOlder: level ', level);
//console.log('growOlder: this.settings: ', this.settings);

        // age child parts
        this.parts.forEach(function (part) {
            totalParts += part.growOlder(level + 1);
        });

        // age this part -- grow new parts?
        if (level < treeSettings.levels) {

            // add a new trunk when old enough
            trunkSettings.newGrowths.forEach(function (newGrowth) {
                var leaves, newTrunk;

                if (newGrowth.age === that.age && (!newGrowth.chance || Math.random() < newGrowth.chance)) {
                    newHeight = config.height * trunkSettings.scale.height;
                    newRadius = config.radius * trunkSettings.scale.radius;

                    rotation = new THREE.Matrix4().makeRotationX(
                        Math.random() * branchRadians * 2 - branchRadians);
                    rotation.multiply(new THREE.Matrix4().makeRotationY(
                        Math.random() * branchRadians * 2 - branchRadians));
                    rotation.multiply(new THREE.Matrix4().makeRotationZ(
                        Math.random() * branchRadians * 2 - branchRadians));

                    // move leaves from this trunk to first new trunk piece,
                    // or add leaves to next added trunks
                    if (!that.hasTrunks) {
                        totalParts -= that.parts.length;
                        leaves = that.parts;
                        that.parts = [];
                        that.hasTrunks = true;
                    } else {
                        leaves = [that.tree.settings.factories.leaf.create(that.tree, {
                            rotation: that.config.rotation
                        })];
                    }

                    // add new trunk segment
                    newTrunk = treeSettings.factories.trunk.create(that.tree, {
                        height: newHeight,
                        radius: newRadius,
                        rotation: rotation
                    });

                    // add the leaves to it
                    leaves.forEach(function (leaf) {
                        newTrunk.parts.push(leaf);
                    });

                    that.parts.push(newTrunk);
                    totalParts += 1;
                }
            });
        }

        // age this trunk
        this.age += 1;

        return totalParts;
    },

    // create geometry scaled by current age
    generate: function (position) {
        var trunkSettings = this.settings,
            unCenter,
            transform,
            newPosition,
            moveToPosition,
            config = this.config,
            height = trunkSettings.growth.height(this.age, trunkSettings.growth.untilAge, 0, config.height),
            radius = trunkSettings.growth.radius(this.age, trunkSettings.growth.untilAge, 0, config.radius),
            newRadius = radius * trunkSettings.scale.radius,
            totalMeshes = 0;

//console.log('generate: trunk: age, height, radius', this.age, height, radius);

//console.log('   config', this.config);
//console.log('generate:    position', position);
        // rotate branch to new orientation and move into position
        transform = new THREE.Matrix4();
        moveToPosition = new THREE.Matrix4().makeTranslation(position.x, position.y, position.z);
        transform.multiply(moveToPosition);
        transform.multiply(config.rotation);
        unCenter = new THREE.Matrix4().makeTranslation(0, 0.5*height, 0);
        transform.multiply(unCenter);

        totalMeshes += this.generateGeometry(height, radius, newRadius, transform);

        // generate child parts
        newPosition = new THREE.Vector3(0, height, 0);
        newPosition.applyMatrix4(config.rotation);
        newPosition.applyMatrix4(moveToPosition);
        this.parts.forEach(function (part) {
            totalMeshes += part.generate(newPosition);
        });

        return totalMeshes;
    },

    generateGeometry: function (height, radius, newRadius, transform) {
        var geom = new THREE.CylinderGeometry(
            newRadius,
            radius,
            height,
            this.settings.generateOptions.radiusSegments,
            this.settings.generateOptions.heightSegments,
            false
        );
        geom.applyMatrix(transform);
        geom.computeFaceNormals();
        geom.computeVertexNormals();

        THREE.GeometryUtils.merge(this.tree.container('trunk'), geom);

        return 1;
    },

    generateMesh: function () {
//console.log('trunk', this.tree.container('trunk'));
        var mesh = new THREE.Mesh(
            this.tree.container('trunk'),
            this.settings.generateOptions.material);
        mesh.castShadow = true;
        mesh.receiveShadow = true;
        return mesh;
    }
};

Tree.Trunk.Factory = function (settings) {
    this.settings = {};
    THREE.extend(this.settings, Tree.Trunk.defaultSettings);
    THREE.extend(this.settings, settings || {});
};

Tree.Trunk.Factory.prototype = {
    create: function (tree, config) {
        return new Tree.Trunk(this.settings, tree, config);
    }
};

// =================================================
// Tree.Leaf
// -------------------------------------------------

Tree.Leaf = function (settings, tree, config) {
    this.settings = settings;
    this.tree = tree;

    this.age = 0;

    this.config = {
        rotation: null
    };

    THREE.extend(this.config, config || {});

    if (!this.config.rotation) {
        this.config.rotation = new THREE.Matrix4();
    }
};

Tree.Leaf.defaultSettings = {
    size: 5,

    growth: {
        size: Tree.Transforms.expDecayFast,
        untilAge: 99
    },

    generateOptions: {
        material: new THREE.MeshPhongMaterial({color: 0x119900, ambient: 0x002200, side: THREE.DoubleSide})
    }
};

Tree.Leaf.prototype = {
    growOlder: function (level) {
        // age this leaf
        this.age += 1;

        return 1;
    },

    // create geometry scaled by current age
    generate: function (position) {
        var config = this.config,
            i,
            numLeaves = 3,
            radiansDelta = 2*Math.PI/numLeaves,
            transform,
            leafTransform,
            totalMeshes = 1,
            leafSize = this.settings.growth.size(
                this.age, this.settings.growth.untilAge, 0, this.settings.size);

        transform = new THREE.Matrix4().makeTranslation(position.x, position.y, position.z);
        transform.multiply(config.rotation);

        leafTransform = new THREE.Matrix4().makeRotationY(radiansDelta);

        totalMeshes += this.generateLeaves(leafSize, transform);
        for (i = 1; i<numLeaves; i++) {
            transform.multiply(leafTransform);
            totalMeshes += this.generateLeaves(leafSize, transform);
        }

        return totalMeshes;
    },

    generateLeaves: function (size, transform) {
        var mesh,
            quarterSize = size/4,
            geom = new THREE.Geometry();
        geom.vertices.push(new THREE.Vector3(0, 0, 0));
        geom.vertices.push(new THREE.Vector3(quarterSize, -quarterSize, quarterSize));
        geom.vertices.push(new THREE.Vector3(size, -quarterSize, 0));
        geom.vertices.push(new THREE.Vector3(quarterSize, -quarterSize, -quarterSize));
        geom.faces.push(new THREE.Face3(0, 1, 2));
        geom.faces.push(new THREE.Face3(0, 2, 3));
        geom.applyMatrix(transform);
        geom.computeFaceNormals();
        geom.computeVertexNormals();

        THREE.GeometryUtils.merge(this.tree.container('leaves'), geom);

        return 1;
    },

    generateMesh: function () {
        var mesh = new THREE.Mesh(
            this.tree.container('leaves'),
            this.settings.generateOptions.material);
        mesh.castShadow = true;
        mesh.receiveShadow = true;
        return mesh;
    }
};

Tree.Leaf.Factory = function (settings) {
    this.settings = {};
    THREE.extend(this.settings, Tree.Leaf.defaultSettings);
    THREE.extend(this.settings, settings || {});
};

Tree.Leaf.Factory.prototype = {
    create: function (tree, config) {
        return new Tree.Leaf(this.settings, tree, config);
    }
};


// =================================================
// Tree.Forest
// -------------------------------------------------

Tree.Forest = function () {
    this.parts = [];
    this.age = 0;
};

Tree.Forest.prototype = {
    growOlder: function () {
        var totalParts = 0;

        this.age += 1;

        this.parts.forEach(function (part) {
            totalParts += part.growOlder();
        });

        return totalParts;
    },

    generate: function (container) {
        var totalMeshes = 0;

        this.parts.forEach(function (part) {
            totalMeshes += part.generate(container);
        });

        return totalMeshes;
    },

    add: function(part) {
        this.parts.push(part);
    }
};

Tree.Forest.Factory = {
    create: function () {
        return new Tree.Forest();
    }
};
