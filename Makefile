
NPM = npm

install:
	$(YNPM) install

jslint: install
	./node_modules/.bin/jslint ./js/index.html ./js/tree.js

test-unit: install
	./node_modules/.bin/mocha \
	    -u tdd --reporter tap \
	    ./test/unit/*.js

package: install jslint test-functional
	yinst_create --clean -i

release: install jslint test-functional
	yinst_create -r --clean



all: install jslint test-unit
